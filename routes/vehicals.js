const express = require('express');
const router = express.Router();
const vehicalController = require('../controllers/vehicals');

router.post('/addVehicals', vehicalController.addVehicals);
router.post('/vehicalsList', vehicalController.vehicalsList);
router.get('/vehicalById/:id', vehicalController.vehicalById);
router.put('/editVehicals/:id', vehicalController.editVehicals);
router.delete('/deleteVehical/:id', vehicalController.deleteVehical);
router.post('/userVehicals', vehicalController.userVehicals);
module.exports = router;