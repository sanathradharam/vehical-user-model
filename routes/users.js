const express = require('express');
const router = express.Router();
const userController = require('../controllers/users');

router.post('/addUsers', userController.addUsers);
router.post('/usersList',userController.usersList);
router.get('/userById/:id', userController.userById);
router.put('/editUser/:id', userController.editUser);
router.delete('/deleteUser/:id', userController.deleteUser);

module.exports = router;