const express = require('express');
const router = express.Router();
const logger = require('morgan');
const configs = require('../configs');

const users = require('./users');
const vehicals = require('./vehicals');

router.use(logger('dev'));

router.use('/users', users);
router.use('/vehicals', vehicals);

router.use('/*', configs.errorResponseController);

module.exports = router;