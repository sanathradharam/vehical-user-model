const chalk = require('chalk');
const moment = require('moment');

const { NODE_ENV, NODE_PORT_ENV } = process.env;
exports.chalk = chalk;

exports.NODE_PORT = NODE_ENV === "production" && NODE_PORT_ENV ? NODE_PORT_ENV : 8888;

exports.errMessage = 'Something went wrong!';

exports.success_status = 200;
exports.error_status = 403;
exports.badRequest_status = 400;
exports.unauthorised_status = 401;
exports.pageLimit = 10;

exports.utcDefault = function utcDefault() {
    let date = new Date()
    return date = moment.utc(date).format();
};

exports.errorResponseController = (req, res) => {
    res.status(404).send({
        code: 0,
        status: 'Error',
        message: 'The server has not found anything matching the Request-URI',
    });
}