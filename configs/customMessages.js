/**
 * Common Messages
 */
 module.exports = {
    // Common Messages
    'EMAIL_PASSWORD_NOT_MATCH': 'Incorrect Email or Password',
    'USER_NOT_EXIST': 'User account does not exist.',
    'EMAIL_ALREADY_EXIST' : 'Email already exists.',   
 }

