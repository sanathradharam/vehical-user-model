var express = require('express');
var mongoose = require('mongoose');
require('dotenv').config();
var configs = require('./configs');
console.log(configs.chalk.yellow('[ wait ] compiling ...'));
var fs = require('fs');
var path = require('path');
var app = express();
var { DB_URL } = process.env;

mongoose.Promise = global.Promise;
mongoose.connect(DB_URL, {  useNewUrlParser: true, useUnifiedTopology: true,}, function (err, data) {
    if (err) {
        console.log();
        console.log(configs.chalk.red('Server failed to connect with database...'));
        console.log(err);
        return;
    } else if (data) {
        console.log();
        console.log(configs.chalk.yellow('Database'), configs.chalk.green('connected successfully...'));
        const models_path = __dirname + '/models';
        fs.readdirSync(models_path).forEach(function (file) {
            if (~file.indexOf('.js')) require(models_path + '/' + file);
        });
        startserver();
    }
});
function startserver() {
    app.use(express.json({
        limit: '50mb'
    }));
    // Parses the text as URL encoded data, extended extends UTF chars
    app.use(express.urlencoded({
        limit: '50mb',
        extended: true,
        parameterLimit: 50000
    }));
    app.use('/api', require('./routes'));

    app.listen(configs.NODE_PORT, function (err) {
        if (err) {
            console.log();
            console.log(configs.chalk.red('App is listening error '), err);
        } else {
            console.log();
            console.log(configs.chalk.green(`App is running on ${app.get('env')} mode at`), configs.chalk.blue(configs.NODE_PORT));
        }
    });
}
