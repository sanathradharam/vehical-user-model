const mongoose = require('mongoose');
const Schema = mongoose.Schema

const userSchema = new Schema({
    //uuid: {type: Number},
    email: { type: String, required: true, unique: { args: true, msg: 'Email already exist' } },
    phone_number: { type: String, required: true, unique: { args: true, msg: 'Phone number already exist' } },
    first_name: { type: String },
    last_name: { type: String },
    location: {
        latitude: {type: Number},
        longitude: {type: Number}
    }
});

module.exports = mongoose.model('users', userSchema);