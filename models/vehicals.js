const mongoose = require('mongoose');
const Schema = mongoose.Schema

const vehicalSchema = new Schema({
    //uuid: {type: Number},
    users: [{ type: Schema.Types.ObjectId, ref: 'users' }],
    vehical_name: {type: String},
    vehical_brand: {type: String},
    vehical_number: {type: Number},
});

module.exports = mongoose.model('vehicals', vehicalSchema);