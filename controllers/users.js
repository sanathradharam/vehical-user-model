const mongoose = require('mongoose');
const Joi = require('joi');
const configs = require('../configs');
const Users = mongoose.model('users');

exports.addUsers = async (req, res, next) => {
    try {
        let receivedBody = req.body;

        const schema = Joi.object({
            //uuid: Joi.number().required(),
            first_name: Joi.string().required(),
            last_name: Joi.string().required(),
            email: Joi.string().required(),
            phone_number: Joi.string().required(),
            location: Joi.object({
                latitude: Joi.number().required(),
                longitude: Joi.number().required(),
            }),
        });

        const validationResult = await schema.validateAsync(receivedBody);

        // const isPhone = await Users.findOne({ phone_number: validationResult.phone_number }).select('_id').exec();
        // if (isPhone) {
        //     return res.json({
        //         status: configs.error_status, message: "Phone number is already registered"
        //     });
        // }

        // const isEmail = await Users.findOne({ email: validationResult.email }).select('_id').exec();
        // if (isEmail) {
        //     return res.json({
        //         status: configs.error_status, message: "Email is already registered"
        //     });
        // }
        const new_usr = new Users({
            //uuid: validationResult.uuid,
            first_name: validationResult.first_name,
            last_name: validationResult.last_name,
            email: validationResult.email,
            phone_number: validationResult.phone_number,
            location: validationResult.location
        });

        console.log(new_usr);

        const data = await new_usr.save();
        console.log('data', data);
        if (data) {
            res.json({
                status: configs.success_status,
                data,
                message: 'user added successfully'
            });
        }
    } catch (error) {
        console.log(error);
        return res.json({ status: configs.error_status, message: configs.errMessage, error: error ? error : "" });
    }
}

exports.usersList = async (req, res, next) => {
    try {
      const pageNum = req.body.pageNum ? req.body.pageNum : 1;
      const limit = req.body.pageLimit ? req.body.pageLimit : 10;
      const skip = limit * (pageNum - 1);
      const search = req.body.search?.trim() ? req.body.search.trim() : null;
  
      const query = {};
  
      const queryListUsers = Users.find(query);
      const qrCountUsers = Users.find(query);
  
      if (limit) {
        queryListUsers.skip(skip).limit(limit);
      }
      const totalUsers = await qrCountUsers.countDocuments().exec();
      const data = await queryListUsers.exec();
      const totalFilteredPage = Math.ceil(totalUsers / limit);
  
      if (data && data.length > 0) {
        return res.json({
          status: configs.success_status,
          data,
          metaData: {
            currentPage: pageNum,
            totalUsers,
            totalFilteredPage,
          },
          message: "Request completed successfully",
        });
      } else {
        return res.json({
          status: configs.success_status,
          data: [],
          message: "No data found",
        });
      }
    } catch (error) {
      console.log(error);
      return res.json({ status: configs.error_status, message: configs.errMessage, error: error ? error : "" });
    }
  };

exports.userById = async (req, res, next) => {
    try {
        const data = await Users.findById(req.params.id)
            .exec();
        if (data) {
            return res.json({
                status: configs.success_status,
                data,
                message: 'Request completed successfully'
            });
        } else {
            return res.json({
                status: configs.success_status,
                data,
                message: 'User not found!'
            });
        }
    } catch (error) {
        console.log(error);
        return res.json({ status: configs.error_status, message: configs.errMessage, error: error ? error : "" });
    }
};

exports.editUser = async (req, res, next) => {
    try {
        let receivedBody = req.body;

        const schema = Joi.object({
            first_name: Joi.string().required(),
            last_name: Joi.string().required(),
            email: Joi.string().required(),
            phone_number: Joi.string().required(),
            location: Joi.number().required()
        });
        const validationResult = await schema.validateAsync(receivedBody);

        const emp = {
            first_name: validationResult.first_name,
            last_name: validationResult.last_name,
            email: validationResult.email,
            phone_number: validationResult.phone_number,
            location: validationResult.location
        };

        const data = await Users.findByIdAndUpdate(req.params.id, { $set: emp }, { new: true }).exec();
        if (data) {
            return res.json({
                status: configs.success_status,
                data,
                message: 'User updated successfully'
            });
        } else {
            return res.json({
                status: configs.success_status,
                data,
                message: 'User not found!'
            });
        }
    } catch (error) {
        console.log(error);
        return res.json({ status: configs.error_status, message: configs.errMessage, error: error ? error : "" });
    }
}

exports.deleteUser = async (req, res, next) => {
    try {
        const schema = Joi.object({
            user_id: Joi.string().required(),
        });
        await schema.validateAsync(req.body);
        const data = await Users.findByIdAndDelete(req.params.id).exec();
        if (data) {
            return res.json({
                status: configs.success_status,
                data,
                message: 'User deleted successfully'
            });
        } else {
            return res.json({
                status: configs.success_status,
                data,
                message: 'User not found!'
            });
        }
    } catch (error) {
        console.log(error);
        return res.json({ status: configs.error_status, message: configs.errMessage, error: error ? error : "" });
    }
};
