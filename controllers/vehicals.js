const mongoose = require('mongoose');
const Joi = require('joi');
const configs = require('../configs');
const Vehicals = mongoose.model('vehicals');
const Users = mongoose.model('users');
const vehicalUsers = mongoose.model('vehicalusers');


exports.addVehicals = async (req, res, next) => {

    try {
        let receivedBody = req.body;

        const schema = Joi.object({
            //uuid: Joi.number().required(),
            vehical_name: Joi.string().required(),
            vehical_brand: Joi.string().required(),
            vehical_number: Joi.number().required(),
            user_id: Joi.string().required()
        });

        const validationResult = await schema.validateAsync(receivedBody);

        const new_veh = new Vehicals({
            //uuid: validationResult.uuid,
            vehical_name: validationResult.vehical_name,
            vehical_brand: validationResult.vehical_brand,
            vehical_number: validationResult.vehical_number,
        });

        const data = await new_veh.save();
console.log('data', data);
        if (data) {
            const usr_list = await Users.findOne({
                _id: validationResult.user_id,
            }).exec();

                const checkVehicalUser = await vehicalUsers.exists({
                    vehical_id: data._id,
                    user_id: validationResult._id,
                });
                if (!checkVehicalUser) {
                    const new_vehical_user = new vehicalUsers({
                        vehical_id: data._id,
                        user_id: data._id,
                    });
                    await new_vehical_user.save();

                    res.json({
                        status: configs.success_status,
                        data,
                        message: 'vehical added successfully'
                    });
                }
        }
    } catch (error) {
        console.log(error);
        return res.json({ status: configs.error_status, message: configs.errMessage, error: error ? error : "" });
    }
}

exports.vehicalsList = async (req, res, next) => {
    try {
      const schema = Joi.object({
        pageNum: Joi.number().default(1),
        pageLimit: Joi.number().default(10),
      });
      const validationResult = await schema.validateAsync(req.body);
  
      const pageNum = validationResult.pageNum;
      const limit = validationResult.pageLimit;
      const skip = limit * (pageNum - 1);
      const orFilter = {};
      const andFilter = {};
      const sort = { "_id": -1 };
      
      let queryDoc = [
        {
          $lookup: {
            from: "users",
            localField: "user_id",
            foreignField: "_id",
            as: "User",
          },
        },
        {
          $unwind: {
            path: "$User",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $match: {
            ...andFilter,
            $or: [orFilter],
          },
        },
        {
          $project: {
            _id: "$_id",
            vehical_name: "$vehical_name",
            vehical_brand: { $ifNull: ["$vehical_brand", ""] },
            vehical_number: { $ifNull: ["$vehical_number", ""] },
            User: {
              _id: "$User._id",
              first_name: { $ifNull: ["$User.first_name", ""] },
              last_name: { $ifNull: ["$User.last_name", ""] },
            },
          },
        },
      ];
  
      let queryCount = queryDoc;
  
      queryDoc = [...queryDoc, ...[{ $sort: sort }, { $skip: skip }, { $limit: limit }]];
  
      queryCount = [
        ...queryCount,
        ...[
          {
            $group: {
              _id: null,
              count: { $sum: 1 },
            },
          },
        ],
      ];
      const data = await Vehicals.aggregate(queryDoc).exec();
      const totalVehicals = await Vehicals.aggregate(queryCount).exec();
      const totalFilteredPage =
        totalVehicals?.length > 0 && totalVehicals[0].count
          ? Math.ceil(totalVehicals[0].count / limit)
          : 0;
      if (data && data.length > 0) {
        return res.json({
          status: configs.success_status,
          data,
          metaData: {
            currentPage: pageNum,
            totalFilteredCount: totalVehicals[0].count,
            totalFilteredPage,
          },
          message: "Request completed successfully",
        });
      } else {
        return res.json({
          status: configs.success_status,
          data: [],
          metaData: {
            currentPage: 1,
            totalFilteredCount: 0,
            totalFilteredPage: 0,
          },
          message: "No data found",
        });
      }
    } catch (error) {
      console.log(error);
      return res.json({
        status: configs.error_status,
        message: configs.errMessage,
        error: error ? error : "",
      });
    }
  };

exports.vehicalById = async (req, res, next) => {
    try {
        const data = await vehicals.findById(req.params.id)
            .exec();
        if (data) {
            return res.json({
                status: configs.success_status,
                data,
                message: 'Request completed successfully'
            });
        } else {
            return res.json({
                status: configs.success_status,
                data,
                message: 'Vehical not found!'
            });
        }
    } catch (error) {
        console.log(error);
        return res.json({ status: configs.error_status, message: configs.errMessage, error: error ? error : "" });
    }
};

exports.editVehicals = async (req, res, next) => {
    try {
        let receivedBody = req.body;

        const schema = Joi.object({
            vehical_name: Joi.string().required(),
            vehical_brand: Joi.string().required(),
            vehical_number: Joi.number.required(),
        });
        const validationResult = await schema.validateAsync(receivedBody);

        const vhi = {
            vehical_name: validationResult.vehical_name,
            vehical_brand: validationResult.vehical_brand,
            vehical_number: validationResult.vehical_number,
        };

        const data = await vehicals.findByIdAndUpdate(req.params.id, { $set: vhi }, { new: true }).exec();
        if (data) {
            return res.json({
                status: configs.success_status,
                data,
                message: 'vehical updated successfully'
            });
        } else {
            return res.json({
                status: configs.success_status,
                data,
                message: 'vehical not found!'
            });
        }
    } catch (error) {
        console.log(error);
        return res.json({ status: configs.error_status, message: configs.errMessage, error: error ? error : "" });
    }
}

exports.deleteVehical = async (req, res, next) => {
    try {
        const schema = Joi.object({
            vehical_id: Joi.string().required(),
        });
        await schema.validateAsync(req.body);
        const data = await vehicals.findByIdAndDelete(req.params.id).exec();
        if (data) {
            return res.json({
                status: configs.success_status,
                data,
                message: 'Vehical deleted successfully'
            });
        } else {
            return res.json({
                status: configs.success_status,
                data,
                message: 'Vehical not found!'
            });
        }
    } catch (error) {
        console.log(error);
        return res.json({ status: configs.error_status, message: configs.errMessage, error: error ? error : "" });
    }
};

exports.userVehicals = async (req, res, next) => {
    try {
        const schema = Joi.object({
            user_id: Joi.string().required(),
            pageNum: Joi.number().default(1),
            pageLimit: Joi.number().default(10),

        });
        const validationResult = await schema.validateAsync(req.body);

        const pageNum = validationResult.pageNum;
        const limit = validationResult.pageLimit;
        const skip = limit * (pageNum - 1);
        const sort = { _id: -1 };

        const filter = {};

        let queryDoc = [
            {
                $lookup: {
                    from: 'users',
                    localField: 'user_id',
                    foreignField: '_id',
                    as: "User"
                }
            },
            {
                $unwind: {
                    path: "$User",
                }
            },
            {
                $lookup: {
                    from: "vehicals",
                    localField: "vehical_id",
                    foreignField: "_id",
                    as: "vehical",
                },
            },
            {
                $unwind: {
                    path: "$vehical",
                },
            },
            {
                $match: {
                    user_id: mongoose.Types.ObjectId(validationResult.user_id),
                    $or: [filter],
                },
            },
            {
                $project: {
                    vehical: {
                        _id: "$vehical._id",
                        vehical_name: "$vehical.vehical_name",
                        vehical_brand: "$vehical.vehical_brand",
                        vehical_number: "$vehical.vehical_number",
                        user: {
                            _id: "$User._id",
                            first_name: "$User.first_name",
                            last_name: "$User.last_name",
                        },
                    },
                },
            }
        ];

        let queryCount = queryDoc;

        queryDoc = [
            ...queryDoc,
            ...[{ $skip: skip }, { $limit: limit }, { $sort: sort }],
        ];

        queryCount = [
            ...queryCount,
            ...[
                {
                    $group: {
                        _id: null,
                        count: { $sum: 1 },
                    },
                },
            ],
        ];

        const data = await vehicalUsers.aggregate(queryDoc).exec();

        const totalVehicals = await vehicalUsers.aggregate(queryCount).exec();
        const totalFilteredPage =
            totalVehicals?.length > 0 && totalVehicals[0].count
                ? Math.ceil(totalVehicals[0].count / limit)
                : 0;

        if (data && data.length > 0) {
            return res.json({
                status: configs.success_status,
                data,
                metaData: {
                    currentPage: pageNum,
                    totalFilteredCount: totalVehicals[0].count,
                    totalFilteredPage,
                },
                message: "Request completed successfully",
            });
        } else {
            return res.json({
                status: configs.success_status,
                data: [],
                metaData: {
                    currentPage: 1,
                    totalFilteredCount: 0,
                    totalFilteredPage: 0,
                },
                message: "No data found",
            });
        }
    } catch (error) {
        console.log(error);
        return res.json({
            status: configs.error_status,
            message: configs.errMessage,
            error: error ? error : "",
        });
    }
};